package easy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * You are given the heads of two sorted linked lists list1 and list2.
 * <br><br>
 * Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.
 * <br><br>
 * Return the head of the merged linked list.
 * <br><br>
 * constraints:
 * <ul>
 *     <li>The number of nodes in both lists is in the range [0, 50].</li>
 *     <li>-100 <= Node.val <= 100</li>
 *     <li>Both list1 and list2 are sorted in non-decreasing order.</li>
 * </ul>
 */
class MergeTwoSortedListsTest {

    private MergeTwoSortedLists test = new MergeTwoSortedLists();

    @Test
    void mergeTwoLists_bothEmpty() {
        assertNull(test.mergeTwoLists(null, null));
    }

    @Test
    void mergeTwoLists_oneEmpty() {
        var result = test.mergeTwoLists(null, new MergeTwoSortedLists.ListNode(2));
        assertNotNull(result);
        assertEquals(2,result.val);
        assertNull(result.next);
    }

    @Test
    void mergeTwoLists_twoSingles() {
        var node1C = new MergeTwoSortedLists.ListNode(1);
        var node1D = new MergeTwoSortedLists.ListNode(2);
        var result = test.mergeTwoLists(node1C, node1D);
        assertNotNull(result);
        assertEquals(1, result.val);
        assertNotNull(result.next);
        assertEquals(2,result.next.val);
        assertNull(result.next.next);
    }

    @Test
    void mergeTwoLists_twoTriples() {
        var node3A = new MergeTwoSortedLists.ListNode(4);
        var node2A = new MergeTwoSortedLists.ListNode(2, node3A);
        var node1A = new MergeTwoSortedLists.ListNode(1, node2A);
        var node3B = new MergeTwoSortedLists.ListNode(4);
        var node2B = new MergeTwoSortedLists.ListNode(3, node3B);
        var node1B = new MergeTwoSortedLists.ListNode(1, node2B);
        var resultTwoLists = test.mergeTwoLists(node1A, node1B);
        assertNotNull(resultTwoLists);
        assertEquals(1, resultTwoLists.val);
        resultTwoLists = resultTwoLists.next;
        assertNotNull(resultTwoLists);
        assertEquals(1, resultTwoLists.val);
        resultTwoLists = resultTwoLists.next;
        assertNotNull(resultTwoLists);
        assertEquals(2, resultTwoLists.val);
        resultTwoLists = resultTwoLists.next;
        assertNotNull(resultTwoLists);
        assertEquals(3, resultTwoLists.val);
        resultTwoLists = resultTwoLists.next;
        assertNotNull(resultTwoLists);
        assertEquals(4, resultTwoLists.val);
        resultTwoLists = resultTwoLists.next;
        assertNotNull(resultTwoLists);
        assertEquals(4, resultTwoLists.val);
        assertNull(resultTwoLists.next);
    }
}