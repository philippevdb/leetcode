package easy;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. The relative order of the elements may be changed.
 * <br><br>
 * Since it is impossible to change the length of the array in some languages, you must instead have the result be placed in the first part of the array nums. More formally, if there are k elements after removing the duplicates, then the first k elements of nums should hold the final result. It does not matter what you leave beyond the first k elements.
 * <br><br>
 * Return k after placing the final result in the first k slots of nums.
 * <br><br>
 * Do not allocate extra space for another array. You must do this by modifying the input array in-place with O(1) extra memory.
 * <br><br>
 * <b>Custom Judge:</b>
 * <br><br>
 * The judge will test your solution with the following code:<br>
 * <hr>
 * int[] nums = [...]; // Input array<br>
 * int val = ...; // Value to remove<br>
 * int[] expectedNums = [...]; // The expected answer with correct length.<br>
 *                             // It is sorted with no values equaling val.
 * <br><br>
 * int k = removeElement(nums, val); // Calls your implementation
 * <br><br>
 * assert k == expectedNums.length; <br>
 * sort(nums, 0, k); // Sort the first k elements of nums<br>
 * for (int i = 0; i < actualLength; i++) {<br>
 *     assert nums[i] == expectedNums[i];<br>
 * }
 * <hr>
 * If all assertions pass, then your solution will be accepted.
 * <br><br>
 * Constraints:
 * <ul>
 *     <li>0 <= nums.length <= 100</li>
 *     <li>0 <= nums[i] <= 50</li>
 *     <li>0 <= val <= 100</li>
 * </ul>
 */
class RemoveElementTest {

    private final RemoveElement test = new RemoveElement();

    @Test
    void removeElement_empty() {
        int[] empty = {};
        assertEquals(0, test.removeElement(empty, 1));
    }

    @Test
    @DisplayName("one")
    void removeElement_OneItem() {
        int[] input0 = {1};
        assertEquals(1, test.removeElement(input0,2));
        assertEquals(1, input0[0]);
        assertEquals(0, test.removeElement(input0, 1));
    }

    @Test
    @DisplayName("four")
    void removeElement_fourItems() {
        int[] input1 = {3,2,2,3};
        assertEquals(2, test.removeElement(input1,3));
        assertEquals(2, input1[0]);
        assertEquals(2, input1[1]);
    }

    @Test
    @DisplayName("eight")
    void removeElement_eightItems() {
        int[] input2 = {0,1,2,2,3,0,4,2};
        assertEquals(5, test.removeElement(input2, 2));
        assertEquals(0, input2[0]);
        assertEquals(0, input2[1]);
        assertEquals(1, input2[2]);
        assertEquals(3, input2[3]);
        assertEquals(4, input2[4]);
    }
    @Test
    @DisplayName("ten")
    void removeElement_tenItems() {
        int[] input2 = {6,5,0,1,2,2,3,0,4,2};
        assertEquals(7, test.removeElement(input2, 2));
        assertEquals(0, input2[0]);
        assertEquals(0, input2[1]);
        assertEquals(1, input2[2]);
        assertEquals(3, input2[3]);
        assertEquals(4, input2[4]);
        assertEquals(5, input2[5]);
        assertEquals(6, input2[6]);
    }
}