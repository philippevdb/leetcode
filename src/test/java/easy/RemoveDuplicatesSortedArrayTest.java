package easy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place such that each unique element appears only once. The relative order of the elements should be kept the same.
 * <br><br>
 * Since it is impossible to change the length of the array in some languages, you must instead have the result be placed in the first part of the array nums. More formally, if there are k elements after removing the duplicates, then the first k elements of nums should hold the final result. It does not matter what you leave beyond the first k elements.
 * <br><br>
 * Return k after placing the final result in the first k slots of nums.
 * <br><br>
 * Do not allocate extra space for another array. You must do this by modifying the input array in-place with O(1) extra memory.
 * <br><br>
 * Custom Judge:
 * <br><br>
 * The judge will test your solution with the following code:
 * <hr>
 * int[] nums = [...]; // Input array<br>
 * int[] expectedNums = [...]; // The expected answer with correct length
 * <br><br>
 * int k = removeDuplicates(nums); // Calls your implementation
 * <br><br>
 * assert k == expectedNums.length;<br>
 * for (int i = 0; i < k; i++) {<br>
 *     assert nums[i] == expectedNums[i];<br>
 * }
 * <hr>
 * Constraints:
 * <ul>
 *     <li>1 <= nums.length <= 3 * 10^4</li>
 *     <li>-100 <= nums[i] <= 100</li>
 *     <li>nums is sorted in non-decreasing order.</li>
 * </ul>
 */
class RemoveDuplicatesSortedArrayTest {

    private RemoveDuplicatesSortedArray test = new RemoveDuplicatesSortedArray();

    @Test
    void removeDuplicates() {
        int[] test1 = {1};
        assertEquals(1, test.removeDuplicates(test1));
        assertEquals(1, test1[0]);
        int[] test2 = {1,1,2};
        assertEquals(2, test.removeDuplicates(test2));
        assertEquals(1, test2[0]);
        assertEquals(2, test2[1]);
        int[] test3 = {0,0,1,1,1,2,2,3,3,4};
        assertEquals(5, test.removeDuplicates(test3));
        assertEquals(0, test3[0]);
        assertEquals(1, test3[1]);
        assertEquals(2, test3[2]);
        assertEquals(3, test3[3]);
        assertEquals(4, test3[4]);
    }
}