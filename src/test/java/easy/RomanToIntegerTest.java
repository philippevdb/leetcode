package easy;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Given a roman numeral, convert it to an integer.<br><br>
 * constraints:
 * <ul>
 *     <li>1 <= s.length <= 15</li>
 *     <li>s contains only the characters ('I', 'V', 'X', 'L', 'C', 'D', 'M').</li>
 *     <li>It is guaranteed that s is a valid roman numeral in the range [1, 3999].</li>
 * </ul>
 */
class RomanToIntegerTest {

    private RomanToInteger romanToInteger = new RomanToInteger();

    @Test
    void romanToInt() {
        String roman1 = "III";
        String roman2 = "LXVIII";
        String roman3 = "MCMXCIV";
        String roman4 = "MCDXXXIV";
        String roman5 = "CDLXXXII";
        assertEquals(3, romanToInteger.romanToInt(roman1));
        assertEquals(68, romanToInteger.romanToInt(roman2));
        assertEquals(1994, romanToInteger.romanToInt(roman3));
        assertEquals(1434, romanToInteger.romanToInt(roman4));
        assertEquals(482, romanToInteger.romanToInt(roman5));
    }

}