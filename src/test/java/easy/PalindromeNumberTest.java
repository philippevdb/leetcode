package easy;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Given an integer x, return true if x is palindrome integer.
 * <br> <br>
 * An integer is a palindrome when it reads the same backward as forward.
 * <ul><li>For example, 121 is a palindrome while 123 is not.</li></ul>
 * Constraints:
 * <ul><li>-2^31 <= x <= (2^31) - 1</li></ul>
 * Could you solve it without converting the integer to a string?
 */
public class PalindromeNumberTest {

    private PalindromeNumber pn = new PalindromeNumber();

    @Test
    void isPalindrome() {
        assertTrue(pn.isPalindrome(1259779521));
        assertTrue(pn.isPalindrome(125979521));
        assertFalse(pn.isPalindrome(1259779421));
        assertFalse(pn.isPalindrome(1259779523));
        assertFalse(pn.isPalindrome(-1259779521));
    }

    @Test
    void convertIntegerToList() {
        List<Integer> result = pn.convertIntegerToList(654321);
        List<Integer> expected = Arrays.asList(1,2,3,4,5,6);
        assertTrue(CollectionUtils.isEqualCollection(expected,result));
    }
}
