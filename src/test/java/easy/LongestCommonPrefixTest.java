package easy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestCommonPrefixTest {

    private LongestCommonPrefix test = new LongestCommonPrefix();

    @Test
    void longestCommonPrefix() {
        String[] strings = {"flower","flow","flight"};
        String result = test.longestCommonPrefix(strings);
        assertEquals("fl",result);
        String[] strings2 = {"dog","racecar","car"};
        String result2 = test.longestCommonPrefix(strings2);
        assertEquals("",result2);
        String[] strings3 = {"watergate", "waterfall","water","watersource","waterspring","waterbottle"};
        String result3 = test.longestCommonPrefix(strings3);
        assertEquals("water",result3);
    }
}