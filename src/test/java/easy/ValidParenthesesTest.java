package easy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 * <br><br>
 * An input string is valid if:
 * <ol>
 *     <li>Open brackets must be closed by the same type of brackets.</li>
 *     <li>Open brackets must be closed in the correct order.</li>
 *     <li>Every close bracket has a corresponding open bracket of the same type.</li>
 * </ol>
 * Constraints:
 * <ul>
 *     <li>1 <= s.length <= 10^4</li>
 *     <li>s consists of parentheses only '()[]{}'.</li>
 * </ul>
 */
class ValidParenthesesTest {

    private ValidParentheses test = new ValidParentheses();

    @Test
    void isValid() {
        assertTrue(test.isValid("()[]{}"));
        assertTrue(test.isValid("{[]}"));
        assertFalse(test.isValid("({)}"));
        assertFalse(test.isValid("()[{}"));
        assertFalse(test.isValid("(]{})"));
        assertFalse(test.isValid("){}("));
        assertFalse(test.isValid("()["));
    }
}