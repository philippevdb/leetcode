package easy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 * <br> <br>
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * <br><br>
 * You can return the answer in any order.
 * <br><br>
 * constraints:
 * <ul>
 *     <li>2 <= nums.length <= 10^4</li>
 *     <li>-10^9 <= nums[i] <= 10^9</li>
 *     <li>-10^9 <= target <= 10^9</li>
 *     <li>Only one valid answer exists.</li>
 * </ul>
 * Can you come up with an algorithm that is less than O(n2) time complexity?
 */
public class TwoSumTest {

    private int[] nums1 = {2,7,11,15};
    private int target1 = 9;
    private int[] nums2 = {-9,-5,-2,0,3,7,8,12,14};
    private int target2 = 6;

    @Test
    void testArray1() {
        int[] twoSum1 = TwoSum.twoSum(nums1,target1);
        List<Integer> result1 = new ArrayList<>();
        IntStream.range(0,twoSum1.length).forEach(i -> result1.add(twoSum1[i]));
        Assertions.assertEquals(Arrays.asList(0,1),result1);
    }

    @Test
    void testArray2() {
        int[] twoSum2 = TwoSum.twoSum(nums2,target2);
        List<Integer> result2 = new ArrayList<>();
        IntStream.range(0,twoSum2.length).forEach(i -> result2.add(twoSum2[i]));
        Assertions.assertEquals(Arrays.asList(2,6),result2);
    }
}
