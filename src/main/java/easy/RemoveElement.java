package easy;

public class RemoveElement {

    /**
     * mistake made when reading exercise, I don't need to order the values in array as well.
     */
    public int removeElement(int[] nums, int val) {
        if (nums.length == 1 && nums[0] != val) return 1;
        int result = nums.length;
        for (int k = 0; k < nums.length; k++) if (nums[k]==val) {nums[k]=51;result--;};
        for (int i = 1; i < nums.length; i++) {
            int key = nums[i];
            inner: for (int j=i-1;j>=0;j--) {
                if (nums[j] > key) {
                    nums[j+1] = nums[j];
                    nums[j] = key;
                } else break inner;
            }
        }
        return result;
    }
}
