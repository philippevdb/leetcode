package easy;

import java.sql.Array;
import java.util.*;

public class RomanToInteger {

    private Map<Character,Integer> conversions;

    public RomanToInteger() {
        conversions = new HashMap<>();
        conversions.put('I',1);
        conversions.put('V',5);
        conversions.put('X',10);
        conversions.put('L',50);
        conversions.put('C',100);
        conversions.put('D',500);
        conversions.put('M',1000);
    }

    public int romanToInt(String s) {
        char[] characters = s.toCharArray();
        int result =  0;
        int lastValue = 0;
        for (int i = 0; i < characters.length; i++) {
            int value = conversions.get(characters[i]);
            if (value > lastValue) {
                result = result - lastValue + (value - lastValue);
                lastValue = value;
                continue;
            }
            result += value;
            lastValue = value;
        }
        return result;
    }
}
