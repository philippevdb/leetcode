package easy;

import java.util.ArrayList;
import java.util.List;

public class PalindromeNumber {

    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        } else if (x < 10) {
            return true;
        }
        List<Integer> digits = convertIntegerToList(x);
        int start = 0;
        int end = digits.size()-1;
        while (start < end) {
            if (digits.get(start) != digits.get(end)) {
                return false;
            }
            start++;
            end--;
        }
        return true;
    }

    protected List<Integer> convertIntegerToList(int x) {
        List<Integer> digits = new ArrayList<>();
        while (x > 0) {
            digits.add((x % 10));
            x /= 10;
        }
        return digits;
    }

}
