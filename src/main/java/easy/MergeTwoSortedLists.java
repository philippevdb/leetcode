package easy;

public class MergeTwoSortedLists {

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null && list2 == null) return null;
        else if (list1 == null) return list2;
        else if (list2 == null) return list1;
        boolean list1LargerOrEqualThan = list1.val <= list2.val;
        return new ListNode(
                list1LargerOrEqualThan ? list1.val : list2.val,
                mergeTwoLists(
                        list1LargerOrEqualThan ? list1.next : list2.next,
                        list1LargerOrEqualThan ? list2 : list1)
        );
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
