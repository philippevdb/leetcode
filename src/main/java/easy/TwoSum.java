package easy;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {

    public static int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> correspondingValues = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int value = nums[i];
            if (correspondingValues.containsKey(value)) {
                return new int[]{correspondingValues.get(value),i};
            } else {
                correspondingValues.put(target-value,i);
            }
        }
        return null;
    }
}
