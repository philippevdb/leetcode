package easy;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Write a function to find the longest common prefix string amongst an array of strings.
 * <br><br>
 * If there is no common prefix, return an empty string "".
 * <br>
 * constraints:
 * <ul>
 *     <li>1 <= strs.length <= 200</li>
 *     <li>0 <= strs[i].length <= 200</li>
 *     <li>strs[i] consists of only lowercase English letters.</li>
 * </ul>
 */
public class LongestCommonPrefix {

    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 1) return strs[0];
        List<String> input = Arrays.asList(strs);
        String shortest = input.stream()
                .min(Comparator.comparing(String::length)).get();
        if (shortest.length() == 0) return shortest;
        input = input.stream().filter(str -> !str.equals(shortest)).collect(Collectors.toList());
        StringBuilder result = new StringBuilder("");
        for (int i=0;i<shortest.length();i++) {
            char currentChar = shortest.toCharArray()[i];
            final int offset = i;
            if (input.removeIf(str -> !str.startsWith(Character.toString(currentChar), offset))) {
                break;
            } else {
                result.append(currentChar);
            }
        }
        return result.toString();
    }
}
