package easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ValidParentheses {

    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (char character : s.toCharArray()) {
            if (character == '(' || character == '{' ||character == '[') {
                stack.push(character);
            } else if (stack.empty()) {
                return false;
            } else if (
                    (character == ')' && stack.pop() == '(')
                    || (character == ']' && stack.pop() == '[')
                    || (character == '}' && stack.pop() == '{')
            ) {
            } else {
                return false;
            }
        }
        return stack.empty();
    }
}
