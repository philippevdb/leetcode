package easy;

public class RemoveDuplicatesSortedArray {

    public int removeDuplicates(int[] nums) {
        if (nums.length == 1) return 1;
        int count = 0;
        for (int i=1; i<nums.length; i++) {
            if (nums[i] != nums[i-1]) {
                count++;
                nums[count] = nums[i];
            }
        }
        return ++count;
    }
}
